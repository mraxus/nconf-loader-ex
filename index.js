'use strict';

function extendWithEnvironmentVariables(obj) {

    var fn = function (v) {

        if (typeof v === 'string' && v.indexOf('$') === 0) {
            return process.env[v.substring(1)] || '(' + v.substring(1) + ' not set)';
        }

        return extendWithEnvironmentVariables(v);
    };

    if (Array.isArray(obj)) {
        return obj.map(fn);
    }
    else if (obj !== null && typeof obj === 'object') {
        var newObj = {};
        Object.keys(obj).forEach(function (k) { newObj[k] = fn(obj[k]); });
        return newObj;
    }
    else if (typeof obj === 'function') {
        return;
    }

    return obj;
}

module.exports.version = require('./package.json')['version'];
module.exports.load = function (configDirName) {

	var fs = require('fs'),
		nconf = require('nconf'),
		path = require('path'),
		environment = process.env.NODE_ENV || 'development',

		baseConfigPath	= path.join(configDirName, 'config.json'),
		envConfigPath	= path.join(configDirName, 'config.' + environment + '.json'),
		localConfigPath	= path.join(configDirName, 'config.' + environment + '.local.json'),

		hasBaseConfig	= fs.existsSync(baseConfigPath),
		hasEnvConfig	= fs.existsSync(envConfigPath),
		hasLocalConfig	= fs.existsSync(localConfigPath);

	if (!hasBaseConfig && !hasEnvConfig)
		throw new Error('Could not find "' + envConfigPath + ' or ' + baseConfigPath + '". Cannot load config.');

	if (hasLocalConfig)	{ nconf.file('local',		{ file: localConfigPath, search: false }); }
	if (hasEnvConfig)	{ nconf.file('environment',	{ file: envConfigPath,	 search: false }); }
	if (hasBaseConfig)	{ nconf.file('base',		{ file: baseConfigPath,	 search: false }); }

	return extendWithEnvironmentVariables(nconf.get());
};
