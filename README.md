# nconf configuration file loader

Loads nconf configuration files from disk.

It looks for *config.json* in the specified directory.
Should *config.x.json* exist, the configuration will be overloaded with the environment config.
Should *config.x.local.json* exist, the configuration will be overloaded with the local config.

x is defined by `NODE_ENV` environment variable (*development* if not set).

It also handles references to environment variables by prefixing a variable name with "$"

See [NCONF](https://npmjs.org/package/nconf) for additional info.


## Usage

There are two requirements to run the script:

- A wellformed json file named *config.json*, *config.x.json* and/or *config.x.local.json* where x is `NODE_ENV` or *development*.
- Point to the directory location of the config file(s).

### Load the configuration

```js
require('nconf-loader').load(__dirname);
```

### Examples

config.json:
```js
{
	"fruit": "pear",
	"drink": "beer",
	"food": "spaghetti"
}
```

config.development.json:
```js
{
	"fruit": "apple",
	"drink": "milk"
}
```

config.development.local.json:
```js
{
	"fruit": "ananas"
}
```

config.production.json:
```js
{
	"fruit": "pineapple",
	"drink": "pina colada"
}
```

config.enve.json:
```js
{
	"fruit": "$FRUIT",
	"drink": "$DRINK"
}
```

app.js:
```js
var config = require('nconf-loader-ex').load(__dirname);

console.log('fruit of choice:', config.fruit);
console.log('favourite drink:', config.drink);
console.log('food of choice:', config.food);
```
The result when running `node app.js`:

	fruit of choice: ananas
	favourite drink: milk
	food of choice: spaghetti

The result when running `NODE_ENV=production node app.js`:

	fruit of choice: pineapple
	favourite drink: pina colada
	food of choice: spaghetti

The result when running `NODE_ENV=enve FRUIT=mango DRINK=powderday node app.js`:

	fruit of choice: mango
	favourite drink: powderday
	food of choice: spaghetti

All of the examples above can be found in the `examples/` folder.