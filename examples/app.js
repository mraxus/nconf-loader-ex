'use strict';

var config = require('../index.js').load(__dirname);

console.log('fruit of choice:', config.fruit);
console.log('favourite drink:', config.drink);
console.log('food of choice:', config.food);