'use strict';

var test, configLoader;

test = require("tap").test;
configLoader = require('../index.js');

test('Load with basic config only', function (t) {

	var config;

	process.env.NODE_ENV = 'dummy';
	config = configLoader.load(__dirname + '/configs');


	t.equal(config.config1.prop1.value, 'test value 1');
	t.equal(config.config2.prop1.value, 'test value 2');
	t.equal(config.config2.prop2.value, 'test value 3');

	t.end();
});
