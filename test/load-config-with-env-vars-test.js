'use strict';

var test, configLoader;

test = require("tap").test;
configLoader = require('../index.js');

test('Load with basic and environment config', function (t) {

	var config;

	// Setup
	process.env.NODE_ENV = 'enve';
	process.env.PORT = 5142;

	// Load config
	config = configLoader.load(__dirname + '/configs');

	// Run tests
	t.equal(config.api.port, process.env.PORT);

	t.end();
});
