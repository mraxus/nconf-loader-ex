'use strict';

var test, configLoader;

test = require("tap").test;
configLoader = require('../index.js');

test('Load with basic and environment config', function (t) {

	var config;

	process.env.NODE_ENV = 'development';
	config = configLoader.load(__dirname + '/configs');

	t.equal(config.config1.prop1.value, 'test value 1');
	t.equal(config.config2.prop1.value, 'develop test value 2');
	t.equal(config.config3.prop1.value, 'develop test value 3');
	t.equal(config.config4.prop1.value, 'develop test value 4');

	t.end();
});
